require('dotenv').config()
const blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');
const moment = require('moment');
const CronJob = require('cron').CronJob;
const config = require('./config.json')

const CANCEL_PD_ON_START = process.env.CANCEL_PD_ON_START === "true";

// Extract keys from .env
function extractKeys(map) {
  const ACCOUNTS_COUNT = 19;
  for (let i = 0; i < ACCOUNTS_COUNT; i++) {
    let c = `C${i.toString().padStart(2, '0')}`;
    if (process.env[c]) {
      let entry = process.env[c].split(",")
      map[entry[0].trim()] = entry[1].trim()
    }
  }
}
let keys_map = {}
extractKeys(keys_map)

// Create accounts map
let accounts = config.accounts.map(item => item.name);
let accounts_map = {}
config.accounts.forEach(item => {
  accounts_map[item.name] = {
    wif: keys_map[item.name],
    routes: item.routes,
  }
});

const CRON_SCHED = process.env.CRON_SCHED || "00 01 00 * * *";
console.log('CRON_SCHED', CRON_SCHED)
const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
console.log('rpc', RPC_URL)
blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

// set vesting routes
config.accounts.forEach(account => {
  let { name } = account;
  let { wif, routes } = accounts_map[account.name];
  routes.forEach(route => {
    let { to_account, share } = route;
    /** set withdraw vesting route */
    let withdraw_percent = parseInt(share) * 100;
    blurt.broadcast.setWithdrawVestingRoute(
      wif,  // active or owner key
      name, // from_account
      to_account, // to_account
      withdraw_percent, // percent (The percent of the withdraw to go to the ‘to’ account.)
      false, // auto_vest (Set to true if the from account should receive the VESTS as VESTS (BLURT POWER), or false if it should receive them as liquid BLURT)
      function(err, result) {
        console.log(err, result);
      }
    );
  })
});

function cancelPowerDown(account) {
  let { name } = account;
  let { wif } = accounts_map[account.name];
  // cancel power down
  blurt.broadcast.withdrawVesting(wif, name, '0.000000 VESTS',
    function(err, result) {
      if (err) {
        console.log('error', err)
        return;
      }
      console.log(`cancelled power down for ${name}`)
    }
  );
}

function powerDown(account) {
  let { name } = account;
  let { wif } = accounts_map[account.name];
  let vesting_shares = account.vesting_shares.replace(" VESTS", "").replace(".", "");
  let delegated_vesting_shares = account.delegated_vesting_shares.replace(" VESTS", "").replace(".", "");
  let withdrawable_vests = (vesting_shares - delegated_vesting_shares) / 1e6
  let to_powerdown_vests = withdrawable_vests.toString() + ' VESTS'

  // power down
  blurt.broadcast.withdrawVesting(wif, name, to_powerdown_vests,
    function(err, result) {
      if (err) {
        console.log('error', err)
        return;
      }
      // no need to broadcast transfer since powerdown are automatically transferred via routes
      console.log(`powered down ${withdrawable_vests}`)
    }
  );
}

if (CANCEL_PD_ON_START) {
  blurt.api.getAccounts(accounts, function (err, result) {
    if (!_.isEmpty(result)) {
      result.forEach(account => {
        let { vesting_withdraw_rate } = account;
        if (vesting_withdraw_rate !== "0.000000 VESTS") {
          cancelPowerDown(account);
        }
      })
    }
  });
}

const job = new CronJob(CRON_SCHED, function() {
  blurt.api.getAccounts(accounts, function (err, result) {
    if (!_.isEmpty(result)) {
      result.forEach(async account => {

        // check if current power down has not liquidated (transferred yet)
        let now = moment.utc();
        console.log('now', now)

        let { next_vesting_withdrawal } = account;
        let withdrawDate = moment.utc(next_vesting_withdrawal);
        console.log('withdrawDate', withdrawDate)

        // compute for delay until withdrawal is executed;
        // (next_vesting_withdrawal - now) => convert diff to ms
        let timedelay = withdrawDate.diff(now);
        console.log('timedelay', timedelay)

        // don't broadcast next powerdown until current powerdown liquid is executed
        if (timedelay > 0) {
          setTimeout(function() {
            powerDown(account);
          }, timedelay)
        } else {
          powerDown(account);
        }

      })
    }
  });
});
console.log('After job instantiation');
job.start();