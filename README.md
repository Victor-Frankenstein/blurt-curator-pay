## How to use it

Create a `config.json` and place all the curator accounts, and routes.

```
{
  "accounts" : [
    {
      "name": "curator1",
      "routes": [
        { "to_account": "account1", "share": 15 },
        { "to_account": "curator1", "share": 85 }
      ]
    },
    {
      "name": "curator2",
      "routes": [
        { "to_account": "account1", "share": 15 },
        { "to_account": "curator2", "share": 85 }
      ]
    }
  ]
}
```

Create a `.env` and place curator account and active key as a CSV.

```
RPC_URL=https://rpc.blurt.world
ACCOUNTS_COUNT=19
CANCEL_PD_ON_START=true
FIRST_POWERDOWN_DELAY=3000
TRANSFER_TIMEOUT=10080000
C00=curation1,active_key
C01=curation2,active_key
C02=curation3,active_key
```

Starting the server for development `npm run dev`

For production, I would suggest using PM2 Library. (Install with `npm install -g pm2`)

Then, run `pm2 start index.js`
