const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RouteSchema = new Schema({
  to_account: {
    type: String,
    required: true,
  },
  percent_share: {
    type: Number,
    required: true,
  }
});

const CuratorRouteSchema = new Schema({
  account: {
    type: String,
    required : true,
  },
  routes: [RouteSchema],
});

module.exports = { CuratorRouteSchema };